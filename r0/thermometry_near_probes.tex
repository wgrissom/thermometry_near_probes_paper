\documentclass[11pt]{article}
% Command line: pdflatex -shell-escape compulse.tex
\usepackage{geometry} 
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{times}
\usepackage{bm}
\usepackage{fixltx2e}
\usepackage[outerbars]{changebar}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{color}
\usepackage{tabularx}
\usepackage{ulem}

%\usepackage{fancyhdr}
%\pagestyle{fancy}
%\fancyhf{}
%\fancyhead[R]{\thepage}
%\renewcommand{\headrulewidth}{0pt}

\usepackage{url}
%\newcommand{\bmag}{$\vert B_1^+\vert$}

\usepackage{hyperref}
\hypersetup{colorlinks=false,urlcolor=blue,linkcolor=blue}

\epstopdfsetup{suffix=} % to remove 'eps-to-pdf' suffix from converted images
%\usepackage{todonotes} % use option disable to hide all comments

\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{,}{,}

\usepackage[noend]{algpseudocode}
\usepackage{relsize}
\usepackage{dsfont}


%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
%\makeatletter
 % \def\tagform@#1{\maketag@@@{[#1]\@@italiccorr}}texhashtexhashtexhash
%\makeatother

\linespread{1.5}
%\setlength{\parindent}{0in}

% the following command can be used to mark changes made due to reviewers' concerns. Use as \revbox{Rx.y} for reviewer x, concern y.
\newif\ifmarkedup
\markeduptrue

\ifmarkedup
	\newcommand{\revbox}[1]{\marginpar{\framebox{\textcolor{blue}{#1}}}}
\else
	\newcommand{\revbox}[1]{}
	\renewcommand{\textcolor}[1]{}
	\renewcommand{\sout}[1]{}
\fi

%\newcommand{\bop}{$\vert B_1^+ \vert$}
\newcommand{\kt}{$k_\textrm{T}$}
\newcommand{\bmap}{$B_1^+$}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}  % tilde symbol
\mathchardef\mhyphen="2D

\begin{document}

\title{Dual echo z-shimmed proton resonance frequency-shift MR thermometry near metallic ablation probes}
\author{Yuxin Zhang$^{1}$ and William A. Grissom$^{*2,3}$}
\date{} % suppress date (JMRI)
\maketitle
\begin{flushleft}
$^1$Department of Biomedical Engineering, Tsinghua University, Beijing, China\\
$^2$Vanderbilt University Institute of Imaging Science, Nashville, TN, United States\\
$^3$Department of Biomedical Engineering, Vanderbilt University, Nashville, TN, United States\\

\par
-------------------------- 

\par
Word Count: Approximately 3000 \\
*Corresponding author: \\
Will Grissom\\
Department of Biomedical Engineering\\
Vanderbilt University\\
5824 Stevenson Center\\
Nashville, TN 37235 USA \\
E-mail: will.grissom@vanderbilt.edu \\
Twitter: @wgrissom

\par Submitted to Magnetic Resonance in Medicine for consideration as a Note. 

\par Grant Support: NIH R21 NS091735.  

\par Running title: z-Shimmed MR Thermometry

\end{flushleft}
\thispagestyle{plain}

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%% Abstract %%%%%%%%%%%%%%%%%%%%%%%

\section*{Abstract} 
{\bf Purpose:}
To improve the precision of proton resonance frequency-shift MR thermometry
near ablation probes, 
by recovering near-probe signals that are typically lost due
to magnetic susceptibility-induced field distortions. 
\\
\\
{\bf Methods:}
A dual-echo gradient-recalled echo sequence was implemented,
in which the first echo was under- or over-refocused in the slice dimension
to recover signal near a probe, 
and the second echo was fully-refocused to obtain signal everywhere else
in the slice. 
A penalized maximum likelihood algorithm was implemented to estimate a 
single temperature map from both echoes. 
Phantom and ex vivo porcine tenderloin experiments 
with and without microwave heating at 3 Tesla
evaluated
how much temperature precision was improved near a microwave ablator 
compared to a conventional
single-echo scan as a function of 
slice and needle orientation in the magnet, and
whether the best z-shim gradient area changed after tissue ablation.
\\
\\
{\bf Results:}
%In all cases, signal recovery with the z-shimmed dual-echo sequence was evident in magnitude images, 
%and temperature precision was improved near the ablation probe. 
The number of near-probe voxels with temperature standard deviation $\sigma > 1$ ${^{\circ}}$C 
was decreased by 44\% in the phantom experiment, averaged across orientations.
In the porcine tenderloin experiment, the best z-shim gradient area was the same before and after ablation.
%In microwave heating experiments, the number of significant heated voxels increased by 18\% and 36\% in the phantom
%and porcine tenderloin, respectively.
%\\
%\\
%{\bf Results:}
%
\\
\\
{\bf Conclusion:}
Dual echo z-shimmed temperature imaging  
can recover signal for more precise temperature mapping near metallic ablation probes. 
\\
\\

\noindent Key words: MR thermometry; interventional MRI; z-shimming; ablation; microwave.

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%% Main Text %%%%%%%%%%%%%%%%%%%%%%%

\section* {Introduction}
%Proton resonance frequency-shift (PRF) MR thermometry has been widely applied to monitor temperature changes during thermal ablation
%\cite{rieke:mrt:2008}. 
%Many different methods have been proposed to address the problems of PRF thermometry like motion artefacts, nonuniform background magnetic field changes, 
%susceptibility problem and rapid anatomic phase variations.
%Generally, multi-baseline methods \cite{vigen:multibaseline:2003,shmatukha:multibaseline2:2006} are robost to periodic motion and anatomic phase variations; 
%referenceless methods \cite{rieke2004referenceless,grissom:regularizedRef:2009,grissom:l1ref:2010} are insensitive to motion, susceptibility changes and background magnetic field changes. 
%Hybrid methods \cite{grissom:hybrid:2010,Gaur:multiecho:2014} combined the multi-baseline and referenceless concepts to show more robustness to motion, anatomical phase and susceptibility.
 
Proton resonance frequency-shift (PRF) MR thermometry has been widely applied to monitor temperature change during thermal ablation
\cite{rieke:mrt:2008}.  
However, in RF, microwave and laser ablations,
the magnetic susceptibility difference between the ablation probe and surrounding tissue 
creates $B_0$ field distortions 
%(Figure \ref{fig:motivation}a) 
that lead to through-plane and in-plane phase gradients through
a voxel that cause signal loss and pile-up,
%(Figure \ref{fig:motivation}b and c),
and prevent accurate and precise temperature monitoring where the thermal dose is highest. 
%There are two kinds of signal loss induced by metal in gradient echo images, through-plane dephasing and in-plane dephasing
%\cite{hargreaves:metal:2011,koch:metal2:2010}. 
%The reason of through-plane dephasing is the difference of larmor frequency through the slice caused by through-plane magnetic field gradient, 
%as shown in figure \ref{fig:motivation}b. 
%Especially near the wire tip or the heat source location, through-plane dephasing is stronger than in-plane dephasing.

\par Several sequences and reconstructions have been proposed to mitigate signal loss and distortions
caused by metallic interventional devices and implants.
The classic method which addresses in-plane distortions in spin echo imaging 
is view angle tilting \cite{cho:vat:1988,Butts:2005aa}.
More recently, MAVRIC and SEMAC imaging have been developed for spin echo imaging near metallic implants \cite{hargreaves:metal:2011,koch:metal2:2010},
and are based on a combination of view angle tilting with slice-dimension phase encoding.
This approach has further been applied to 
$T_1$-based thermometry \cite{weber:mrm:2016} near implants.
Multi-frequency reconstruction methods have been developed to alleviate in-plane 
distortions in center-out radial gradient-recalled echo (GRE) sequences \cite{corasor:2013}.
Through-plane signal loss in GRE sequences has been addressed in functional MRI
using z-shimming, in which multiple images are acquired with different slice-dimension phase
encode gradient moments that are tuned to compensate through-plane phase gradients in 
brain tissue above the sinuses and ear canals \cite{frahm:mrm:1988,constable1999zshimcomp,constable1995zshim}. 
Quadratic phase excitation pulses have also been applied for this purpose, 
but come at the cost of decreased signal away from the recovered regions \cite{cho1992ge}. 
The white marker method is an application of z-shimming to 
interventional MRI, for the purpose of localizing an interventional device \cite{seppenwoolde:whitemarker:2003}.
In that method, an acquisition is performed with an unbalanced slice rewinder, 
which suppresses signals from all tissues except those adjacent to the device. 
The white marker method has been incorporated into a real-time dual-echo SSFP sequence,
to simultaneously obtain a wire image and a background tissue image to overlay it on \cite{campbell:dual:2014}.

\par In this work we apply z-shimming to MR thermometry, 
to recover temperature precision 
near an ablation probe. 
Analogous to the dual-echo device visualization method \cite{campbell:dual:2014},
we propose a dual-echo sequence in which the first echo is z-shimmed to obtain 
high signal near the probe,
and the second echo is fully-refocused to obtain high signal everywhere else. 
An associated penalized likelihood approach is used 
to reconstruct a single temperature map from both echoes.
Phantom and ex vivo experiments were performed with and without microwave heating
to characterize signal recovery and temperature precision improvement using the sequence 
and reconstruction,
compared to a conventional single-echo MR thermometry scan. 
Overall the method aims 
to improve the precision of PRF-shift MR thermometry
near metallic ablation probes, where the temperature and thermal dose are highest. 
%by recovering signal near the probe that is typically lost due
%to magnetic susceptibility-induced field distortions.
% End your introduction with a clear purpose that matches that presented in your abstract. Do not end the introduction with an overview of your study or its design.

\section*{Methods}

\subsection*{Pulse Sequence}

Figure \ref{fig:sequence} shows the proposed dual echo sequence. 
Tissue signal lost near a wire or probe due to magnetic susceptibility differences is recovered
in the first echo by setting 
the first rephasing gradient trapezoid after slice selection to $p$\% of its fully-refocused area. 
This reduces the linear component of the
through-slice phase accrued between the excitation pulse and the first echo, 
so that the first echo shows increased signal near the probe.
$p$ is set to maximize signal recovery in a region-of-interest (ROI) near the probe. 
Since the z-shim will reduce signal away from the probe, 
a second slice refocusing gradient is placed 
between the first and second echoes with ${(100-p)\%}$ of the full refocusing gradient area.
The second echo has a longer conventional 
$\textrm{TE}\approx T_2^*$ 
echo time \cite{rieke:mrt:2008}, 
while the first echo is sampled earlier because 
a) this reduces signal loss due to in-plane field gradients, 
and b) temperature is highest near an ablation probe, so 
there will be additional signal attenuation due primarily to increasing 
$T_1$ with temperature, leading to a shorter optimal TE in that area. 
% WAG: I'm not sure that 'bc there will be high temperature there' is a good reason to 
% sample near the probe first, since precision is not a function of phase.
%to capture full first echo will contain low signal distant from the wire, the 
%second echo 
The echoes are measured with the same readout gradient polarity so that chemical shift 
occurs in the same direction \cite{gaur:mrm:2015}.
The sequence was implemented on a 3T scanner (Philips Achieva, Philips Healthcare).
Unless otherwise stated below, scans with the sequence used 
8 ms z-shimmed TE, 16 ms fully-refocused TE, 
30 ms TR, 25${^\circ}$ flip angle, 160$\times$160 mm$^{2}$ FOV, 
176$\times$176 reconstructed matrix size, 4 mm slice thickness 
and 220 Hz pixel bandwidth. 
The body coil was used for excitation and a 32-channel head coil was used for signal
reception.

%The first echo is used to get the near-probe signal because of two reasons. 
%First, the temperature is higher near the probe so the first echo time can be relatively short to accumulate enough temperature-induced phase. 
%Second, short echo time can minimize the signal loss due to in-plane gradients. 

%Signals away from the probe are fully refocused by  
%The second echo then images the rest of the tissue away from the probe, and has a more typical PRF echo time (approximately T2*). 

\subsection*{Temperature estimation}
The temperature maps from the two echoes must be combined, either by a signal-weighted average,
or another combination that emphasizes the 
temperature value coming from the echo with the strongest signal at each voxel. 
Here, the multi-echo hybrid multibaseline and referenceless thermometry 
algorithm \cite{Gaur:multiecho:2014,grissom:hybrid:2010} was 
used.
The algorithm jointly fits the following image model to the two echo images:
\begin{center}
\begin{equation}
y_{j}=\left(\sum\limits^{N_{b}}_{b=1}x_{b,j}w_{b}\right)e^{\imath2\pi(\left\{\bm{Ac}\right\}_{j}+f_{j})\textrm{TE}}+\epsilon_{j},
\end{equation}
\end{center}
where ${{y_{j}}}$ is the signal of the ${j}$th image voxel, 
${x_{b,j}}$ is the ${b}$th complex image from the baseline library containing $N_b$ images,
${w_{b}}$ is the weight for baseline image $b$, 
$\bm{A}$ and $\bm{c}$ are a matrix of polynomial phase basis 
functions and its coefficient vector, respectively, 
${f_{j}}$ is the heat-induced frequency shift at location $j$,
TE is the echo time of the modeled echo, and 
${\epsilon_{j}}$ is complex Gaussian noise. 
This additive Gaussian noise model is fit to the two echo images using a descent algorithm
that minimizes a regularized least-squares cost function, 
with sparsity regularization applied to the temperature map since the heating is expected to be localized
to the probe.
The sparsity penalty also helps separate hot spot phase shifts from the background phase shifts 
that are modeled by the polynomial.
The algorithm returns a single estimate of the baseline weights, the polynomial coefficients, and the
frequency shift map that apply to both echoes. 
Further algorithm details are provided in Refs. \cite{Gaur:multiecho:2014} and \cite{grissom:hybrid:2010},
and MATLAB code for the algorithm and an example porcine tenderloin 
dual-echo microwave heating dataset can be downloaded from \href{https://bitbucket.org/wgrissom/thermometry_near_probes_paper}{https://bitbucket.org/wgrissom/thermometry\_near\_probes\_paper}. 
Once fit, the frequency shifts are converted to temperature according to:
\begin{equation}
\Delta T_j = \frac{2\pi f_j}{c \gamma B_0},
\end{equation}
where $c = 0.01$ ppm/$^{\circ}$C, $\gamma$ is the gyromagnetic ratio, and $B_0$ is the main
field strength in Tesla. 
In the present work, this method was applied with a single baseline image and a zeroth-order polynomial.

\subsection*{Experiments}
\paragraph{Signal recovery versus $p$}
To illustrate how the recovered signal near the probe depends on $p$, 
images of an agar phantom were acquired with a nitinol microwave probe embedded in it, without heating.
The probe (Eco Inc., Nanjing, China) had a 3 mm diameter and a metal heat source ring located 1 cm from its tip. 
In this experiment, the slice was positioned to approximately coincide with the heat source ring.
Images were acquired with $p$ values ranging from 30 to 160\%,
and a spin echo image was acquired in the same slice position, 
to evaluate how well z-shimming closed the signal loss hole around the probe.
%\textcolor{red}{WAG Where was the slice positioned with respect to the end of the probe?} 

\paragraph{Recovery of temperature precision}
%Temperature SNR is defined as the temporal standard deviation of dynamic temperature maps without heating, 
%which can be used to measure the precision and uncertainty of the estimated temperature maps.
To measure how much temperature precision was recovered near 
the same microwave ablation probe as above,
100 dynamic images of the agar phantom were acquired without heating.  
Four sets of images were acquired with the probe parallel and perpendicular to $B_0$, 
and with the slice parallel and perpendicular to the probe. 
For perpendicular slice-probe orientations, the slice was positioned approximately at the heat source ring plane,
and for parallel slice-probe orientations the slice was positioned in the same plane as the probe. 
%\begin{enumerate}
%\item Probe parallel to ${B_{0}}$ field direction with imaging slice perpendicular to the probe right at the heat source plane;
%\item Probe parallel to ${B_{0}}$ field direction with imaging slice parallel to the probe with the probe lied in the slice;
%\item Probe perpendicular to ${B_{0}}$ field direction with imaging slice perpendicular to the probe right at the heat source plane;
%\item Probe perpendicular to ${B_{0}}$ field direction with imaging slice parallel to the probe with the probe lied in the slice.
%\end{enumerate}
Temperature maps were estimated from both echoes and from the conventional fully-refocused
echo, and temperature standard deviation was calculated across time for each voxel to assess precision.

\paragraph{Agar phantom heating}
To characterize recovery of actual temperature maps during heating,
four agar phantoms were heated using a microwave generator (Eco Inc., Nanjing, China) 
and the nitinol microwave probe described above, at 25 Watts for five minutes.
During the heating, four temperature scans were acquired with the same four probe and slice orientations as above.
Temperature maps were estimated from both dual-echo and conventional fully-refocused single-echo gradient echo images.

\paragraph{\textit{Ex-vivo} porcine tenderloin heating}
Temperature standard deviation and heating experiments were conducted on a piece of porcine tenderloin
to evaluate signal recovery in actual tissue, 
and whether the best value of $p$ stayed the same after heating.
The scans used $p$ = 50\%, and the same sequence parameters as above
except with 50 ms TR, 21${^\circ}$ flip angle, 100$\times$120 mm$^{2}$ FOV, 
240$\times$240 reconstructed matrix size and 542 Hz pixel bandwidth. 
Images were acquired with the probe parallel to ${B_{0}}$ and with the imaging slice
perpendicular to the probe approximately in the heat source ring plane.
The sample was heated at 25 Watts for three minutes and then cooled down for two minutes. 
During heating, dynamic images were acquired to measure the temperature change. 
To evaluate whether the best value of $p$ remained constant after heating, 
images with different ${p}$ values were acquired before and right after heating.
In addition, to investigate the precision of the recovered temperature maps with the same ${p}$ before and after heating, 
100 dynamic images were acquired both before heating and after cooling down to calculate temperature 
standard deviation maps. 

\section*{Results}
\subsection*{Signal recovery versus $p$}
Figure \ref{fig:magimgs}a shows that 
the value of $p$ determines how much and where signal is recovered
in the first echo image,
and Figure \ref{fig:magimgs}b plots the mean voxel magnitudes
in sum-of-squares combinations of the dual-echo images as a function of $p$,
in a near-probe ROI defined as the region where the fully-refocused second echo
image had less than 70\% of its mean intensity elsewhere in the phantom. 
The magnitudes are normalized by the mean signal in that same ROI in the 
second echo image.
%The magnitudes are normalized by the mean signal elsewhere in the phantom. 
The plot shows an optimal value of $p$ = 140\%. 
%For reference, the normalized magnitude in the ROI of the second echo image alone was 0.09.
Images acquired with $p$ = 140\% were used to form the 
sum-of-squares-combined image in Figure \ref{fig:magimgs}c. 
% recovered signal magnitudes
%We manually selected $p = 140$\% in this case, 
%and Figure \ref{fig:sequence}c shows the first and second echo images and their 
%sum-of-squares combination. 
From the images in Figure \ref{fig:magimgs}c it is evident that the first echo image 
shows enhanced signal around the probe,
while the second echo gives a conventional gradient echo image with signal everywhere else. 
When the magnitudes of the first echo and second echo images were combined by sum-of-squares,  
the size of the signal loss hole was reduced:
the full-width-at-half-max (FWHM) of the hole in the spin echo image was 4.4 mm,
the FWHM of the hole in the conventional fully-refocused echo image was 11.6 mm,
and the FWHM of the combined image was 7.5 mm, 
which was 35\% narrower than the conventional image. 

\subsection*{Recovery of temperature precision}
Figure \ref{fig:Tsnr}a shows images and temperature maps across probe and slice orientations. 
%This figure respectively shows the results of the situations 
%when the probe was perpendicular or parallel to the ${B_{0}}$ field orientation 
%and when the slice position was parallel or perpendicular to the probe.
The values of $p$ are indicated for each case.
Magnitudes of the first echo and second echo images and the sum-of-squares 
combined dual-echo images are shown on the left.
Recovery is evident in each orientation
and is reflected by the relative ROI signal in the combined images, defined as above for Figure \ref{fig:magimgs}b.
Temperature standard deviation maps 
derived from fully-refocused single echo and dual-echo images
are shown to the right,
and the number of low precision voxels with $\sigma > 1{^{\circ}}$ C is indicated for each case.
Averaged across orientations, 
the number of low precision voxels decreased by 42\% in the dual-echo maps.
The white arrows in the $\sigma$ maps in the fourth row (parallel needle/perpendicular slice) point to a representative voxel 
where standard deviation was reduced from 6.7${^{\circ}}$ C to 2.2${^{\circ}}$ C using the dual-echo scan,
and the temperature in that voxel is plotted across dynamics in Figure \ref{fig:Tsnr}b.
The conventional fully-refocused single-echo sequence yielded a maximum error of 15.0${^{\circ}}$ C
in this voxel, while the z-shimmed dual-echo sequence had a maximum absolute error of 2.8${^{\circ}}$ C.

\subsection*{Agar phantom heating}
Figure \ref{fig:heating} shows temperature maps at peak heat for each of the four probe/slice orientation profiles.
Single-echo temperature maps have large temperature holes around the metallic 
probe due to low signal there.
The proposed dual-echo method shows more complete temperature maps near the probe 
compared to the conventional fully-refocused single-echo PRF method, 
as reported by the number of voxels in the ROI with temperature rise greater than 1$^{\circ}$ C. 

\subsection*{\textit{Ex-vivo} porcine tenderloin heating}
Figure \ref{fig:pork} shows the results of \textit{ex-vivo} heating experiments on porcine tenderloin.
First and second echo magnitude images of the dual-echo sequence are shown in Figure \ref{fig:pork}a
before heating, immediately after heating, and after a two-minute cool-down. 
When the tissue around the needle was heated, 
its signal intensity decreased in both first and second echo images. 
After the cool-down period, the signal intensity mostly recovered. 
Although the recovered signal intensity decreased with increasing temperature,
the best value of $p$ remained 50\%,
as shown in Figure \ref{fig:pork}d. 
Temperature standard deviation maps (Figure \ref{fig:pork}b) demonstrate 
that the dual echo maps contain fewer high-$\sigma$ voxels near the metallic probe 
both before heating and after the two-minute cool-down. 
%But when the tissue is heated to high temperature, 
%the uncertainty estimated around the probe would be larger due to the lower signal intensity.
Figure \ref{fig:pork}c shows temperature maps at 30 s, 90 s and 150 s during heating. 
The dual echo maps show more complete hot spots near the probe,
compared to the conventional single-echo maps, as evidenced by the number of reported 
voxels with temperature rises greater than 1$^{\circ}$ C.

\section* {Discussion}
\subsection*{Summary of results}
This work presented a dual echo sequence with z-shimming  
and an associated penalized likelihood approach to estimate a single temperature map from both echoes, 
which can recover signal for more precise temperature mapping near metallic ablation probes and wires. 
We used the first echo to measure near-probe signals
by changing the slice rephasing lobe immediately preceeding it to ${p\%}$ of its full-refocusing area. 
Signals away from the probe were measured with a typical PRF thermometry echo time,
after fully refocusing the slice using a second slice refocusing gradient placed between the two echoes. 
Temperature precision in a phantom was measured in multiple probe and slice orientations
using dynamic images acquired without heating,
which showed that the number of voxels near the probe with greater than 1$^{\circ}$ C uncertainty was reduced 44\% on average
using two echoes versus one. 
Phantom heating experiments then showed that the number of heated
voxels with temperatures increases greater than 1$^{\circ}$ C 
near the probe increased by 18\% on average,
which further confirmed that the dual-echo method effectively filled in the signal loss hole near the probe.
Finally, experiments with a piece of porcine tenderloin showed that the best value of the z-shim gradient area 
did not change with temperature in actual tissue,
even though signal intensity near the probe changed with temperature.
Interestingly, the best value of $p$ in the porcine tenderloin was approximately opposite that  
of the phantom experiments with the same probe and slice orientation (140\% for the phantom versus 50\% for the porcine tenderloin).
Though we made an effort to line up the slice with the metal heat source ring in both cases,
this difference may have been due 
to opposite slice position errors with respect to the ring between the two cases. 

\subsection*{Limitations of the study}
%\subsection*{Practical approaches to tune the refocusing gradient area}
%Contrast of the image varies with refocusing area and reduced slice refocusing area is used to get near-probe signal by compensating through-plane phase. 
%If the compensation is insufficient, signal from features in the background will dominate the dephased image; 
%on the contrary, excessive refocusing area will lead to reduced recovered signal intensity near probe.  
Although the proposed method was able to consistently recover significant signal 
and reduce temperature uncertainty near an ablation probe,
there remain some unexplored aspects that may lead to improvements.
In this work, 
the values of ${p}$ were chosen to maximize the sum-of-squares 
combined signal magnitude near the probe
on a case-by-case basis for each imaging scenario, 
using pre-scan images acquired with across a range of $p$ values.
While temperature precision does depend on the underlying MR signal magnitude in a voxel \cite{rieke:mrt:2008},
signal magnitude is an indirect indicator of the final temperature precision, 
and it may be possible to improve our results
by directly tuning $p$ to maximize temperature precision. 
Our signal magnitude-based tuning approach was chosen 
in consideration of the long scan and processing times that would be required to perform online temperature
precision calculations for every $p$. 
 It may be possible to use a fixed value of $p$ across all tissues 
 for a given probe, field strength, and position and orientation of the slice and probe, 
 but this was not evaluated here. 
Another consideration to be explored is the best timing of the first (z-shimmed) echo, 
and it may be possible to improve the near-probe temperature precision from that echo by tuning it for a given tissue type.
%Although ${p}$ only needs to be selected once before thermal therapy, 
%the performance will be improved by automatically deciding the optimized refocusing area \cite{Heberlein:autoZshim:2005}. 
%Besides, when the slice position is not perpendicular to ${B_{0}}$ field direction, 
%the recovery of temperature is more evident in one direction than the other due to the inhomogeneous through-plane gradients around the metallic probe. 

\subsection*{Possible extensions}
The simplicity of the proposed sequence could make many extensions and improvements
possible. 
If a center-out radial imaging readout is used instead of a Cartesian readout,
the present method could be combined with off-resonant reconstruction to simultaneously reduce in-plane 
distortions near the probe \cite{corasor:2013}.
%The method may also enable absolute thermometry in tissuesnear the probe 
%by enabling smooth interpolation
%of the hot spot into the probe region, which could then by be 
Many ablation probes have thermocouples in their tips to provide temperature measurements
at the point of ablation; 
the improved near-probe temperature measurements yielded by the proposed method
may make it possible to accurately interpolate temperature over the remaining tissue next to the probe
 whose signal is not fully recovered by the proposed method, using the thermocouple temperature
 as a data point in the middle of that tissue. 
 In that case, the combination of PRF measurements
 with thermocouple measurements could make absolute temperature monitoring possible. 
 Tuning of $p$ based on signal magnitude recovery 
 may be accelerated using methods developed for z-shimmed functional MRI, 
 such as a procedure that fits a k-space slice profile to a set of images acquired across a sparse set of $p$ values \cite{marshall:aeaz:2009}. 
 Another possible extension that may obviate case-by-case tuning of $p$ 
 would be to add more echoes to the sequence, each with a different value of $p$. 
Different values of $p$ recover different regions,
so this acquisition could more effectively fill in the signal loss region.
These echoes could further be acquired with a high pixel bandwidth to minimize 
in-plane distortions, and estimating temperature from all echoes jointly could 
compensate for precision lost due to the increased bandwidth \cite{Gaur:multiecho:2014}.
This may further enable simultaneous water/fat separation, for monitoring ablations in fatty tissues such as
breast and diseased liver.

\section* {Conclusion}
A dual echo z-shimmed temperature imaging sequence and an associated temperature reconstruction algorithm were described, 
which can recover signal for more precise temperature mapping near metallic ablation probes.

%\section* {Appendix}

%\pagebreak

\bibliographystyle{cse}
\bibliography{thermometry_near_probes}

\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%    figures    %%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}
%\begin{center}
%\includegraphics*[width=14cm]{motivation.eps}
%\end{center}
%\caption{Background and motivation. 
%(a) Three dimensional field inhomogeneity map ${\delta{B}}$ induced by metallic probe. 
%The through-plane gradients are strong near wire tip (black arrow).
%(b) Principle of through-slice signal loss. 
%(c) Magnitude of Spin Echo image and Gradient Echo image near the tip of a 3mm nitinol probe in an agar phantom.}
%\label{fig:motivation}
%\end{figure}

\begin{figure}
%\begin{center}
%\includegraphics*[width=15cm]{../figures/Sequence.eps}
%\end{center}
\caption{The proposed dual echo z-shimmed PRF-shift thermometry pulse sequence.}
\label{fig:sequence}
\end{figure}

\begin{figure}
%\begin{center}
%\includegraphics*[width=15cm]{../figures/MagnitudeImages.eps}
%\end{center}
\caption{(a) Images with different values of $p$ show different levels and distribution 
of signal recovery near the probe. 
(b) Mean voxel magnitudes in a near-probe ROI in sum-of-squares combined dual-echo images, 
normalized by the mean intensity in the same ROI in the second echo image.
The ROI was defined as the region around the probe where the second echo
image had less than 70\% of the mean intensity of the phantom.
(c) Magnitude images of the first echo ($p = 140$\%), the second echo and the combined two-echo images. 
The region near the probe is zoomed in for each case.
A spin echo image is also shown to illustrate how close the recovery
comes to closing the hole around the probe.}
\label{fig:magimgs}
\end{figure}

\begin{figure}
%\begin{center}
%\includegraphics*[width=16cm]{../figures/TemperatureSNR.eps}
%\end{center}
\caption{
(a) Signal recovery and temperature map precision with different probe and slice orientations, without heating. 
The value of $p$ used is reported for each case.
Magnitude images of the first echo and second echo images are shown in the first two columns, 
and their combined magnitude is shown in the third column. 
For the combined images, 
mean signal in a low-signal ROI around the wire is reported as a percentage of the mean ROI signal
in the second echo images.
The positions of the perpendicular slices 
are indicated by the yellow lines in the magnitude images in the parallel slice images.
Maps of temperature standard deviation ($\sigma$) across time 
are shown in the fourth and fifth columns, for the fully-refocused echo alone and for the dual-echo
scan. 
The number of voxels with $\sigma > 1$ $^{\circ}$C is reported on a  
zoomed-in $\sigma$ map for each case; smaller numbers are better.
(b) The temperature curve over time for a recovered voxel 
(indicated by the white arrow in the parallel needle/perpendicular slice $\sigma$ map) 
illustrates the range of temperature errors with single- and dual-echo scans.}
\label{fig:Tsnr}
\end{figure}

\begin{figure}
%\begin{center}
%\includegraphics*[width=15cm]{../figures/heating.eps}
%\end{center}
\caption{
Temperature maps at five minutes
in each needle and slice orientation.
The positions of the perpendicular slices 
are indicated by the yellow lines in the magnitude images in the parallel slice images.
The number of voxels in the ROI near the probe
with temperature rise $>$1 $^{\circ}$C is reported for both methods with different orientations; larger numbers are better.}
%Maps from the proposed dual-echo z-shimmed scan contain 
%more significant temperature closer to the probe,
%compared to the conventional fully-refocused single echo maps.}
\label{fig:heating}
\end{figure}

\begin{figure}
%\begin{center}
%\includegraphics*[width=15cm]{../figures/pork.eps} %
%\end{center}
\caption{Porcine tenderloin heating experiment. 
(a) First and second echo images from the dual-echo sequence, with ${p= 50\%}$,
before and immediately after heating, and after a three minute cool-down.
(b) Temperature standard deviation across 100 dynamics before heating and after a two-minute-cool-down.
The number of voxels with $\sigma > 1$ $^{\circ}$C is reported; smaller numbers are better.
(c) Temperature maps at 30 s, 90 s and 150 s of the 180 s ablation at 25 Watts,
using single echo and dual echo methods. 
%The dual echo method's maps are more complete near the probe; 
The number of voxels with temperature rise $>$1 $^{\circ}$C is reported for both methods at each time point;
larger numbers are better.
(d) Mean signal recovery in a near-probe ROI as a function of $p$, before and after heating.}
\label{fig:pork}
\end{figure}


%\renewcommand\thefigure{S\arabic{figure}} 
%\setcounter{figure}{0}  



\end{document}