% Process pork data
close all;clear;clc;
load ExampleImage
 imdata=ExampleImage(:,:,:,20);
 baseline=ExampleImage(:,:,:,1);
  %% parameters
 [nx,ny,Ne,ndyn,nc]=size(imdata);
TE1=8E-3; % seconds, first TE
TE2=16E-3; % seconds, second TE time
B0 = 3;             % Main magnetic field : T
Gamma = 2.675e8;    % gyromagnetic ratio unit : rad/s/T
alpha = 0.01e-6;   % ppm/degrees C
order=0; % polynomial order for hybrid model. 0 = drift only
laml1 = 0.1; % temperature sparsity parameter

 %% thermometry
te=[TE1,TE2];
% process conventional single-echo image
msingle = thermo_hybrid_l1_me(conj(imdata(:,:,2)), order, laml1, reshape(squeeze(conj(baseline(:,:,2))),[size(imdata,1)*size(imdata,2),1]),TE2, 0.1*(Gamma*B0*alpha));
% process dual-echo image (the proposed method)
mdual = thermo_hybrid_l1_me(conj(imdata), order, laml1, reshape(squeeze(conj(baseline)),[size(imdata,1)*size(imdata,2)*Ne,1]), te, 0.1*(Gamma*B0*alpha));

figure(1),
subplot(2,2,1),imagesc(abs(imdata(:,:,2))),colormap(gray),title 'Single echo image';colorbar;axis image;
subplot(2,2,2),imagesc(sqrt(abs(imdata(:,:,1)).^2+abs(imdata(:,:,2)).^2)),colormap(gray),title 'Sum-of-squares dual-echo image';colorbar;axis image
subplot(2,2,3),imshow((msingle/(Gamma*B0*alpha)),[0 40]),colormap(jet),title('Single echo temp (\Delta^{\circ}C)');colorbar
subplot(2,2,4),imshow(mdual/(Gamma*B0*alpha),[0 40]),colormap(jet),title('Dual echo z-shimmed temp (\Delta^{\circ}C)');colorbar

