function [m,A,c,f,wts,dnorm] = thermo_hybrid_l1_me(img, order, lam, L, te, tempthresh, dnormmask, modeltest, mask)

% function [m,A,c,f,wts] = thermo_hybrid_nounwrap(img, order, lam, L, dnormmask)
%
% 
% Hybrid Multibaseline/L0 Referenceless Thermometry
% 
% Multicoil compatible
%
% Also does simple multibaseline, and referenceless. 
%
% Inputs:
%   img    [Nx,Ny,Nc]    Nc complex coil images
%   order  1             Polynomial order
%   lam    1             L1 penalty weight
%   L      [Nx*Ny*Nc,Nl] Multibaseline image library
% 
% Outputs:
%   m      [Nx,Ny]      Common phase difference
%   A      [Nx*Ny,Np]   Polynomial matrix
%   c      [Np,Nc]      Polynomial coeffs
%   f      [Nx,Ny,Nc]   Estimated baseline images
%   wts    [Nl,1]       Final baseline image weights

LtL = real(L'*L);
disp('Performing multiecho hybrid thermometry');

% if order > 1
%     error 'Poly order must be 0 or 1'
% end

% construct polynomial matrix
[dim(1),dim(2),Ne,Nc] = size(img);
[yc,xc] = meshgrid(linspace(-1,1,dim(2)), linspace(-1,1,dim(1)));
yc = yc(:);
xc = xc(:);
A = [];
for yp = 0:order
  for xp = 0:(order-yp)
    A = [A (xc.^xp).*(yc.^yp)];
  end
end

if ~exist('dnormmask','var')
    dnormmask = true(dim);
elseif isempty(dnormmask)
    dnormmask = true(dim);
end
if ~exist('modeltest','var')
    modeltest = false; % switch to test model accuracy
end

phs = reshape(angle(img),[prod(dim) Ne Nc]);  
mag = reshape(abs(img),[prod(dim) Ne Nc]);

% get initial f,c
% get initial baseline estimate
[~,~,wts] = f_update(mag,zeros(size(phs)),zeros(size(phs)), ...
    zeros(prod(dim),1),Nc,abs(L),abs(L)'*abs(L),1);
f = reshape(L*wts,[size(L,1)/Nc/Ne Ne Nc]);
fphs = angle(f);
% if not pure multi-baseline, get initial coeffs by reweighted-l1
% if ~isempty(A)
%     for ii = 1:Nc
%         [~,~,c(:,ii)] = l0polyfit_fd(img(:,:,ii).*exp(-1i*angle(reshape(f(:,ii),dim))),...
%             order,ones(dim),0,0.01);
%     end
%     Ac = A*c;
% else
    Ac = zeros(prod(dim),Nc);
    c = zeros(size(A,2),1);
%end

% get initial m value as weighted mean residual phase across coils
% mask = sum(mag,2) ~= 0;
% m = sum(mag.*angle(exp(1i*phs).*conj(f.*exp(1i*Ac))),2)./sum(mag,2);
% m(~mask) = 0;
% % mask m, to improve robustness to noise outside the object
% mask = sum(mag,2) > 0.15*max(sum(mag,2));
% m(~mask) = 0;
% 
% % force positivity
% m(m < -pi/4) = m(m < -pi/4) + 2*pi;
% m(m < 0) = 0;
m = zeros(prod(dim),1);

% Normalize data to get on scale with sparsity penalty
dnorm = dnorm_update(L,Nc,dnormmask);
if dnorm ~= 0
    mag = mag / dnorm;
    f = f / dnorm;
    L = L / dnorm;
    LtL = LtL / dnorm / dnorm;
else
    disp(['Warning: normalization = 0, so not applied. This can ' ...
        'happen when the object has been masked. lam ' ...
        'may need tweaking.']);
end

tic

%cost = cost_eval(mag,phs,Ac,m,f,fphs,lam,Nc);
cost = -1;
cost_old = -1;

m = 0*m;c = 0*c; % actually, let's start with all zeros

nl1iters = 0;
while cost_old-cost > 0.0001*cost_old || cost_old == -1

    % update poly coeffs
    if ~isempty(A)
        c = c_update(mag,phs,A,c,Ac,m,f,fphs,Nc,te);
        Ac = A*c;
    end
    
    % update m
    if ~modeltest
        if ~exist('mask','var')
            m = m_update(mag,phs,Ac,m,f,fphs,lam,Nc,te,false);
        else
            mnew = m_update(mag,phs,Ac,m,f,fphs,0,Nc,te,false);
            m(mask) = mnew(mask);
        end
    end
    %cost_eval(mag,phs,Ac,m,f,fphs,lam,Nc)
    
    % update image estimate
    [f,fphs] = f_update(mag,phs,Ac,m,Nc,L,LtL,te);
    
    % update cost
    cost_old = cost;
    cost = cost_eval(mag,phs,Ac,m,f,fphs,lam,Nc,te);
    %disp(['ML cost: ' num2str(p1) '. Sparsity cost: ' num2str(p2) '.']);
    
    nl1iters = nl1iters + 1;  
    
end
printf('l1-penalized iterations: %f\n',nl1iters);

cost = -1;
cost_old = -1;
if ~modeltest
    mask = m > tempthresh;
else
    mask = true(dim);
end
m(~mask) = 0;

nmiters = 0;
while cost_old-cost > 0.0001*cost_old || cost_old == -1

    % update poly coeffs
    if ~isempty(A) && ~modeltest
        c = c_update(mag,phs,A,c,Ac,m,f,fphs,Nc,te);
        Ac = A*c;
    end
    
    % update m
    mnew = m_update(mag,phs,Ac,m,f,fphs,0,Nc,te,modeltest);
    m(mask) = mnew(mask);
    
    % update image estimate
    if ~modeltest
        [f,fphs] = f_update(mag,phs,Ac,m,Nc,L,LtL,te);
    end
    
    % update cost
    cost_old = cost;
    cost = cost_eval(mag,phs,Ac,m,f,fphs,0,Nc,te);
    
    nmiters = nmiters + 1;
    
end
toc


printf('masked iterations: %f\n',nmiters);

m = reshape(m,dim);
f = reshape(f,[dim Ne Nc]);
if dnorm ~= 0
  f = f*dnorm;
end

% 
% Evaluate cost
%
function p = cost_eval(mag,phs,Ac,m,f,fphs,lam,Nc,te)

% REDFLAG THIS WON'T WORK with MULTICOIL
p = 1/2*(real(f(:)'*f(:)) + mag(:)'*mag(:) - 2*sum(abs(mag(:).*f(:)).*cos(kron(te(:),Ac(:) + repmat(m(:),[Nc 1])) + fphs(:) - phs(:))));
p = p + lam*sum(m(:)); % l1 norm

%
% New normalization scheme
%
function dnorm = dnorm_update(L,Nc,mask)

Ns = size(L,1)/Nc;
coilsum = 0;

for ii = 1:Nc
  coilsum = coilsum + abs(L((ii-1)*Ns+1:ii*Ns,:)).^2;
end
tmp = mean(sqrt(coilsum),2);
dnorm = median(tmp(mask)); % mean across library images
                           % -> same normalization across motion cycle

% 
% Update common phase difference
%
function m = m_update(mag,phs,Ac,m,f,fphs,lam,Nc,te,modeltest)

gradsum = 0;hesssum = 0;

for ii = 1:Nc

    for jj = 1:length(te) % loop over echoes
    
        t1 = abs(mag(:,jj,ii).*f(:,jj,ii));
        t2 = Ac(:,ii)*te(jj) + m*te(jj) + fphs(:,jj,ii) - phs(:,jj,ii);
        t3 = sin(t2);
        t4 = mod(t2+pi,2*pi)-pi;
        t5 = t1.*t3;
        
        gradsum = gradsum + t5*te(jj);
        hesssum = hesssum + t5./t4*te(jj)^2;
  
    end
    
end
  
tmp = (gradsum+lam)./hesssum;
tmp(isnan(tmp)) = 0;
tmp(isinf(tmp)) = 0;
if ~modeltest % enforce non-negativity constraint
    m = max(m - tmp,0);
else % do not enforce constraint, since we are looking at precision
    m = m - tmp;
end

%m(isnan(m)) = 0;

% 
% Update background coefficients
% 
function c = c_update(mag,phs,A,c,Ac,m,f,fphs,Nc,te)

%for ii = 1:5
for ii = 1:Nc
  
    % reset first and second derivative sums for this coil
    gradsum = 0; hesssum = 0;
    
    for jj = 1:length(te)
  
        t1 = abs(mag(:,jj,ii).*f(:,jj,ii));
        t2 = Ac(:,ii)*te(jj) + m*te(jj) + fphs(:,jj,ii) - phs(:,jj,ii);
        t3 = sin(t2);
        t4 = mod(t2+pi,2*pi)-pi;
        t5 = t1.*t3;
        
        gradsum = gradsum + t5*te(jj);
        hesssum = hesssum + t5./t4*te(jj)^2;
        
        %c(:,ii) = c(:,ii) - (A'*bsxfun(@times,t5./(t4+eps),A))\(A'*t5);
        
        %c(:,ii) = c(:,ii) - (A'*bsxfun(@times,t1,A))\(A'*t5); % gives same
        %result but inverted matrix only changes if f changes. how about
        % further assuming that abs(mag(:,ii).*f(:,ii)) ~ abs(mag)^2?
        
    end
    
    hesssum(isnan(hesssum)) = 0;
    hesssum(isinf(hesssum)) = 0;
    gradsum(isnan(gradsum)) = 0;
    gradsum(isinf(gradsum)) = 0;
    c(:,ii) = c(:,ii) - (A'*bsxfun(@times,hesssum,A))\(A'*gradsum);
    
end
%Ac = A*c;
%end

c(isnan(c)) = 0;

%
% Update image estimate
%
function [f,fphs,wts] = f_update(mag,phs,Ac,m,Nc,L,LtL,te)

% note that we force all coils' coeffs to be the same here, which
% may not make sense in situations where the coil array is
% flexible and coils are moving with respect to each other

% Library L is assumed to have dims [Nxy*Ne*Nc Nlib]
Ne = length(te);
if size(L,2) > 1 % if more than one library image: REDFLAG: Not tested!
    
    % set up constraints
    Ceq = ones(1,size(L,2));
    beq = 1;
    
    % set up cost
    c = -real((mag(:).*exp(1i*phs(:)-1i*(kron(te(:),Ac(:)+double(repmat(m,[Nc 1]))))))'*L)';
    
    % solve
    options = optimset;options.MaxIter = 100000;
    wts = quadprog(LtL,c,[],[],Ceq,beq,zeros(size(L,2),1),...
        ones(size(L,2),1),[],options);
    
    % get f
    f = reshape(L * wts, [size(L,1)/Nc/Ne Ne Nc]);
    fphs = angle(f);
    
else
    
    % only one baseline, so weight vector = [1];
    f = reshape(L, [size(L,1)/Nc/Ne Ne Nc]);
    fphs = angle(f);
    
    wts = 1;
    
end


